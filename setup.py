from setuptools import setup


setup(
    name="pypsn",
    version="0.0.1",
    description="PosiStageNet parser",
    long_description="Basic library for interfacing with the PosiStageNet format, more information on the format see https://posistage.net/",
    license="MIT",
    author="vanous",
    author_email="noreply@nodomain.com",
    packages=["pypsn"],
)
